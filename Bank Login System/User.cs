﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CENTRAL_BANK_LOGIN_SYSTEM.classes
{
    public class User
    {
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int BVN  { get; set; }
    }
}
