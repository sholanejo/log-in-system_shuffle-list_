﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CENTRAL_BANK_LOGIN_SYSTEM.classes
{
    public static class Application
    {
        public static void Welcome()
        {
            Console.WriteLine("WELCOME TO THIS MINI APP BVN SYSTEM FOR CBN\n");
            Console.WriteLine("PLEASE, PRESS 1 TO LOG IN TO YOUR  ACCOUNT\nPRESS 2 TO ENROLL FOR YOUR BVN");
            string WelcomeOptions = Console.ReadLine();
            while (WelcomeOptions != "1" && WelcomeOptions != "2")
            {
                Console.WriteLine("Please enter a valid input...\nPress 1 TO LOG IN TO YOUR  ACCOUNT\nPRESS 2 TO ENROLL FOR YOUR BVN");
                WelcomeOptions = Console.ReadLine();
            }


            if (WelcomeOptions == "1")
            {
                LoginEnroll.LoginMethod();
            }
            else
            {
                LoginEnroll.EnrollMethod();
                            
            }
        }
    }
}
