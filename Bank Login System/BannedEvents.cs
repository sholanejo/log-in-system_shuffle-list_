﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CENTRAL_BANK_LOGIN_SYSTEM.classes
{
    public class BannedEvents
    {
        public delegate void BannedEventsEventHandler(object source, EventArgs args);
        public event BannedEventsEventHandler BannedEvent;

        public void Banned(BannedUserModel user)
        {
            Console.WriteLine("....");
            Thread.Sleep(3000);

        }
        protected virtual void OnBannedUser()
        {
            if (BannedEvent != null)
                BannedEvent(this, EventArgs.Empty);
        }
    }
    
}
