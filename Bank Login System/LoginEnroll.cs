﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CENTRAL_BANK_LOGIN_SYSTEM.classes
{
    
    public static class LoginEnroll
    {
        
        private static Database Database = new Database();

        public static void LoginMethod()
        {
            Console.WriteLine("Please fill in your details correctly to check your BVN\n");
            Console.WriteLine("Please enter your Email Address");
            string logMail = Console.ReadLine().ToLower();
            while (validEmail(logMail))
            {
                Console.WriteLine("Please enter a valid email");
                logMail = Console.ReadLine().ToLower();
            }
            Console.WriteLine("Please enter your Password");
            string password = Console.ReadLine();
            while(string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("Please enter a valid password");
                password = Console.ReadLine();
            }

            if(logMail == "james@gmail.com" || logMail == "bill@gmail.com" || logMail == "jane@gmail.com")
            {
                Console.Beep();
                Console.Beep();
                Console.Beep();
                

                Console.WriteLine("SORRY, YOU ARE BANNED FROM USING THIS SERVICE!!!");
                return;
            }

            var querysyntax = from auth in Database.Users()
                              select auth;
            foreach(var A in querysyntax)
            {
                if (A.Email == logMail && A.Password == password)
                {
                    Console.WriteLine("Welcome {0} Your BVN is {1}", A.FullName, A.BVN);
                          
                }
                else if (A.Email == logMail || A.Password != password)
                {
                    Console.WriteLine("USERNAME AND OR PASSWORD IS INCORRECT!!!");
                    LoginMethod();
                }
                else if(A.Email != logMail && A.Password != password)
                {
                    Console.WriteLine("CREDENTIALS DOES NOT EXIST!! PLEASE ENROLL FOR BVN");
                    Application.Welcome();
                }               
            }
        }

        public static void EnrollMethod()
        {
            Console.WriteLine("Welcome to the Enrollment section of this app\nPlease fill the form below correctly to enroll for BVN");
            Console.WriteLine("Full Name(FirstName LastName)");
            string fullName = Console.ReadLine().Trim();
            while(validName(fullName))
            {
                Console.WriteLine("Please enter a valid Fullname (Firstname Lastname)");
                fullName = Console.ReadLine().Trim();
            }
            Console.WriteLine("Please enter your password");
            string password = Console.ReadLine();
            while(string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("Please enter a valid password");
                password = Console.ReadLine();
            }
            Console.WriteLine("Please enter your email address(name@domain.com)");
            string email = Console.ReadLine().ToLower();
            while(validEmail(email))
            {
                Console.WriteLine("Please enter a valid email address(name@domain.com)");
                email = Console.ReadLine().ToLower();
            }
            var querysyntax = from emailCheck in Database.Users()
                              where emailCheck.Email.Equals(email)
                              select emailCheck.Email;
            foreach (var E in querysyntax)
            {
                Console.WriteLine("Sorry {0} cannot enroll more than once for BVN", E);
                return;
            }            
           Console.WriteLine("Congratulations {0} on a successfull BVN Enrollment. Your email is {1} and your BVN is {2}", fullName, email, GetNextInt64());

        }

        private static bool validName(string name)
        {
            Regex regex = new Regex(@"^[a-zA-Z]+\s[a-zA-Z]+$");
            if (regex.IsMatch(name))
                return false;
            return true;
        }
        private static bool validEmail(string email)
        {
            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);
            if (regex.IsMatch(email))
                return false;
            return true; 
        }
        public static long GetNextInt64()
        {
            var bytes = new byte[sizeof(Int64)];
            RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
            Gen.GetBytes(bytes);
            long random = BitConverter.ToInt64(bytes, 0);
            string pos = random.ToString().Replace("-", "").Substring(0, 10);
            return Convert.ToInt64(pos);
        }
    }
}
