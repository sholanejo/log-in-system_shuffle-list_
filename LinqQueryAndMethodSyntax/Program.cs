﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LinqQueryAndMethodSyntax
{
    public static class Program
    {
       
        static Dictionary<char, string> character = new Dictionary<char, string>() 
            {
                {')', "1" },
                {'(', "2" },
                {'*', "3" },
                {'&', "4" },
                {'^', "5" },
                {'%', "6" },
                {'$', "7" },
                {'#', "8" },
                {'@', "9" },
                {'!', "0" }
            };
        static void Main(string[] args)
        {
            Console.WriteLine("1→ ), 2 → (, 3 → *, 4→&, 5 →^, 6 →%, 7 →$, 8 →#, 9 →@, 0 →!");
            Console.WriteLine("please enter any of the alpha numeric characters from the above options to get their corresponding numbers");
            string inputString = Console.ReadLine();

            Console.WriteLine("QUERY SYNTAX\n");
                var querySyntax = from A in inputString
                                    where character.ContainsKey(A)
                                    select character[A];

                foreach (var B in querySyntax)
                {
                    Console.Write(B);
                }


            Console.WriteLine("\nWhere in methodSyntax-----");
            var methodSyntax = inputString.Where(d => character.ContainsKey(d)).Select(d => character[d]);
            foreach (var e in methodSyntax)
            {
                Console.Write(e);
            }
            
                

        }


    }
}

