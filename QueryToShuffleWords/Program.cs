﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QueryToShuffleWords
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("This ConsoleApp Shuffles a sorted list of words\n");
            Console.WriteLine("Please enter the list of words you want to SHUFFLE separated by a comma.");        
            string input = Console.ReadLine();
            List<string> result = input.Split(',').ToList();
            var randomGenerator = new Random();
            var methodSyntax = result.OrderBy(shuffle => randomGenerator.Next());
            Console.WriteLine("Method syntax for the Shuffled words\n");
            Console.WriteLine($"Shuffled list of inputed words: {string.Join(", ", methodSyntax)}");
            
            Console.WriteLine();

            Console.WriteLine("\nQuery Syntax for Shuffled words");
            var querySyntax = from word in result
                              orderby randomGenerator.Next()
                              select word;
            Console.WriteLine($"Shuffled List of inputed Words: {string.Join(", ", querySyntax)}");

        }
    }          
}

